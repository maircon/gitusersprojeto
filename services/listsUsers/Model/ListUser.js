const mongoose = require('mongoose')
const schema = mongoose.Schema

const ListUserSchema = new schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  users: [{
    _id: { type: mongoose.SchemaTypes.ObjectId},
    login: { type: String },
    nome: { type: String },
    bio: { type: String },
    localidade: { type: String },
    html_url: { true: String },
    tags: { type: Array }
  }]
})

const ListUser = mongoose.model('ListUser', ListUserSchema)
module.exports = ListUser