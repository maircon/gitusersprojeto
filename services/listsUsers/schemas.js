const userProfileOutput = {
	type: 'object',
	properties: {
		_id: { type: 'string' },
		email: { type: 'string' }
	}
}
  
const insertGitUserSchema = {
	params:{
		type: 'object',
		required: ['gitLogin'],
		properties: {
			gitLogin: { type: 'string' }
		}
	},
}

const insertListSchema = {
	body: {
		type: 'object',
		required: ['listName'],
		properties: {
			listName: { type: 'string' }
		}
	}
}

const findListSchema = {
	params: {
		type: 'object',
		required: ['listId'],
		properties: {
			listId: {type: 'string'}
		}
	}
}

module.exports = {
	insertGitUserSchema,
	insertListSchema,
	findListSchema
}