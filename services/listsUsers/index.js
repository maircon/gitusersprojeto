const { insertListSchema, findListSchema, insertUsersOnListSchema } = require('./schemas')
const listUserService = require('./service')
const gitUsers = require('../gitUsers/service')
const errors = require('../../utils/errors')

module.exports = async function (fastify, opts) {

	fastify.register(async function (fastify) {
		fastify.addHook('preHandler', fastify.authPreHandler)
		fastify.get('/', {}, findAllListsHandler)
		fastify.post('/', { schema: insertListSchema }, insertListHandler)
		fastify.get('/:listId', { schema: findListSchema }, findListHandler)
		fastify.put('/:listId', { schema: findListSchema }, updateListHandler)
		fastify.delete('/:listId', { schema: findListSchema }, deleteListHandler)
		fastify.post('/:listId/gitUser/:gitUserId', { schema: insertUsersOnListSchema }, insertUsersOnListHandler)
		fastify.delete('/:listId/gitUser/:gitUserId', { schema: insertUsersOnListSchema }, deleteUsersFromListHandler)		
		fastify.put('/:listId/gitUser/:gitUserId', {}, updateUserOnListHandler)
	})

	fastify.setErrorHandler(function (error, request, reply) {
		switch (error.message) {
			case errors.INVALID_TOKEN:
			case errors.LOGIN_ERROR:
				reply.code(401)
				break
			case errors.INTERNAL_ERROR:
				reply.code(500)
				break
			default:
				reply.code(500)
		}
		reply.send(error)
	})
}

module.exports[Symbol.for('plugin-meta')] = {
	decorators: {
	  fastify: [
			'authAdmPreHandler',
			'authPreHandler',
			'jwt'
		]
	}
}

async function findAllListsHandler (req, reply) {
	const list = await listUserService.findAll()
	return { lists: list }
}

async function insertListHandler (req, reply) {
	const listName = { name: req.body.listName }
	const listResult = await listUserService.insertList(listName) 
	return { list : listResult }
}

async function findListHandler (req, reply) {
	const listId = req.params.listId
	const listResult = await listUserService.findById(listId)
	return { list: listResult }
}

async function insertUsersOnListHandler (req, reply) {
	const gitUserId = req.params.gitUserId
	const listId = req.params.listId
	const bodyTags = req.body.tags
	let userInfo = await gitUsers.findById(gitUserId)
	if(userInfo){
		userInfo = userInfo.toObject()
		userInfo.tags = bodyTags
		const result = await listUserService.insertUserOnList(listId, userInfo)
		return { list: result }
	} else {
		throw new Error('Usuario do git não existe')
	}
}

async function updateListHandler (req, reply) {
	const listId = req.params.listId
	const listName = req.body.listName
	const result = await listUserService.updateList(listId, listName)
	return { list: result }
}

async function deleteListHandler (req, reply) {
	const listId = req.params.listId
	const result = await listUserService.deleteList(listId)
	return { list: result }
}

async function deleteUsersFromListHandler (req, reply) {
	const listId = req.params.listId
	const gitUserId = req.params.gitUserId
	const result = await listUserService.deleteUserOnList(listId, gitUserId)
	return { list: result }
}

async function updateUserOnListHandler (req, reply) {
	const listId = req.params.listId
	const gitUserId = req.params.gitUserId
	const userBody = req.body
	const result = await listUserService.updateUserOnList(listId, gitUserId, userBody)
	return { list: result }
}