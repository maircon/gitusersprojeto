const listUser = require('./Model/ListUser')

exports.insertList = async (name) => {
	try {
		const user = new listUser(name)
		const result = await user.save()
		return result
	} catch (e) {
		if (e.code === 11000){
			throw new Error('Usuário do Git já cadastrado')
		} else {
			throw e
		}
	}
}

exports.findById = async (listId) => {
	try {
		const users = await listUser.findById(listId)
		return users
	} catch (e) {
		throw e
	}
}

exports.deleteList = async (listId) => {
	try {
		const list = await listUser.findByIdAndDelete(listId)
		return list
	} catch (e) {
		throw e
	}
}

exports.updateList = async (listId, name) => {
	try {
		const list = await listUser.findOneAndUpdate({ _id: listId }, { name: name }, { new: true })
		return list
	} catch (e) {
		throw e
	}
}

exports.findAll = async () => {
	try {
		const users = await listUser.find({})
		return users
	} catch (e) {
		throw e
	}
}

exports.insertUserOnList = async (listId, userInfo) => {
	try {
		const resultList = await listUser.findOneAndUpdate({ _id: listId },
			{ $push: { users: userInfo } },
			{ new: true })
		return resultList
	} catch (e) {
		throw e
	}
}

exports.updateUserOnList = async (listId, gitUserId , body) => {
	try {
		const resultList = await listUser.findOneAndUpdate(
			{ _id: listId, 'users._id': gitUserId },
			{ $set: {
				'users.$.nome': body.name,
				'users.$.bio': body.bio,
				'users.$.localidade': body.location,
				'users.$.tags': body.tags
				} 
			},
			{ new: true })
		return resultList
	} catch (e) {
		throw e
	}
}

exports.deleteUserOnList = async (listId, gitUserId) => {
	try {
		const resultList = await listUser.findOneAndUpdate({ _id: listId },
			{ $pull: { users: { _id: gitUserId } } },
			{ new: true })
		return resultList
	} catch (e) {
		throw e
	}
}

