  
const insertGitUserSchema = {
	body: {
		type: 'object',
		required: ['gitLogin'],
		properties: {
			gitLogin: { type: 'string' }
		}
	}
}

const deleteGitUserSchema = {
	params: {
		type: 'object',
		required: ['gitUserId'],
		properties: {
			gitUserId: { type: 'string'}
		}
	}
}
  
module.exports = {
	insertGitUserSchema,
	deleteGitUserSchema
}