const gitUser = require('./Model/GitUser')

exports.insertGitUser = async (body) => {
	try {
		const user = new gitUser(body)
		const result = await user.save()
		return result
	} catch (e) {
		if (e.code === 11000){
			throw new Error('Usuário do Git já cadastrado')
		} else {
			throw e
		}
	}
}

exports.findAll = async () => {
	try {
		const users = await gitUser.find({})
		return users
	} catch (e) {
		throw e
	}
}

exports.findById = async (gitUserId) => {
	try {
		const user = await gitUser.findById(gitUserId)
		return user
	} catch (e) {
		throw e
	}
}

exports.deleteUsers = async (gitUserId) => {
	try {
		const user = await gitUser.findByIdAndDelete(gitUserId)
		return user
	} catch (e) {
		throw e
	}
}