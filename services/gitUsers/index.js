const { insertGitUserSchema, deleteGitUserSchema } = require('./schemas')
const gitUserService = require('./service')
const errors = require('../../utils/errors')
const axios = require('axios');

module.exports = async function (fastify, opts) {
	fastify.get('/users', {}, findAllUsersHandler)
	fastify.register(async function (fastify) {
		fastify.addHook('preHandler', fastify.authAdmPreHandler)
		fastify.get('/', {}, findGitHubUsersHandler)
		fastify.post('/', { schema: insertGitUserSchema }, insertGitUsersHandler)
		fastify.delete('/:gitUserId', { schema: deleteGitUserSchema }, deleteUsersHandler)		
	})
	
	
	fastify.setErrorHandler(function (error, request, reply) {
		switch (error.message) {
			case errors.INVALID_TOKEN:
			case errors.LOGIN_ERROR:
				reply.code(401)
				break
			case errors.INTERNAL_ERROR:
				reply.code(500)
				break
			default:
				reply.code(500)
		}
		reply.send(error)
	})
}

module.exports[Symbol.for('plugin-meta')] = {
	decorators: {
	  fastify: [
		'authAdmPreHandler',
		'authPreHandler',
		'jwt'
	  ]
	}
}

async function findGitHubUsersHandler (req, reply) {
	const { data, status } = await axios.get('https://api.github.com/users')
	if (status === 200){
		return { gitUsers: data }
	}
}

async function insertGitUsersHandler (req, reply) {
	console.log(req.body)
	const userLogin = req.body.gitLogin
	let responseGit = {}
	try {
		responseGit = await axios.get(`https://api.github.com/users/${userLogin}`)
	} catch (error) {
			if (error.message = 'Not Found') {
				throw new Error('Usuário não existe no github')
			}
	}
	
	const { status, data } = responseGit
	if (status === 200) {
		const gitUserBody = {
			login: data.login,
			nome: data.name,
			bio: data.bio,
			localidade: data.location,
			html_url: data.html_url
		}
		const insertResult = await gitUserService.insertGitUser(gitUserBody)
		return { result: insertResult }
	} else {
		return { result: 'Ocorreu um problema ao se conectar!' }
	}
}

async function findAllUsersHandler (req, reply) {
	const users = await gitUserService.findAll()
	return { users: users }
}

async function deleteUsersHandler (req, reply) {
	console.log(req.params)
	const userId = req.params.gitUserId
	const users = await gitUserService.deleteUsers(userId)
	return { users: users }
}