const mongoose = require('mongoose')
const schema = mongoose.Schema

const GitUserSchema = new schema({
  login: {
    type: String,
    required: true,
    unique: true,
  },
  nome: {
    type: String,
  },
  bio: {
    type: String,
  },
  localidade: {
    type: String,
	},
	html_url: {
		type: String
	}
})

const GitUser = mongoose.model('GitUser', GitUserSchema)
module.exports = GitUser