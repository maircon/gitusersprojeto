const {
	loginSchema,
  registerSchema,
	validateTokenSchema,
} = require('./schemas')

const userService = require('./service')
const errors = require('../../utils/errors')

module.exports = async function (fastify, opts) {
	fastify.post('/login', { schema: loginSchema }, loginHandler)
	fastify.post('/register', { schema: registerSchema }, registerHandler)

	fastify.setErrorHandler(function (error, request, reply) {
		switch (error.message) {
			case errors.INVALID_TOKEN:
			case errors.LOGIN_ERROR:
				reply.code(401)
				break
			case errors.INTERNAL_ERROR:
				reply.code(500)
				break
			default:
				reply.code(500)
		}
		reply.send(error)
	})
}

async function loginHandler (req, reply) {
	const { email, password } = req.body
	const result = await userService.login(email, password)
	return { jwt: this.jwt.sign({ _id: result._id, user_type: result.user_type }), user: result}
}

async function registerHandler (req, reply) {
	const userInfo = req.body
	const result = await userService.register(userInfo)
	return { user: result }
}