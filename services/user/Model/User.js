const mongoose = require('mongoose')
const schema = mongoose.Schema
const bcrypt = require('bcrypt')

const UserSchema = new schema({
  email: {
    type: String,
    required: true,
    unique: true,
    validate: {
			validator: function (v) {
				return /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(v)
			},
			message: props => `${props.value} não é email válido`
		}
  },
  cpf: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
		required: true,
		select: false
  },
  user_type: {
    type: Number,
    required: true,
    default: 0
  }
})

UserSchema.pre('save', async function (next) {
  const hash = await bcrypt.hash(this.password, 10)
  this.password = hash

  next()
})

UserSchema.statics.findByCredentials = async (email, password) => {
	let user = await User.findOne({ email: email }).select('+password')
	if (!user) {
			throw new Error('Email ou Senha incorreta')
	}
	const isPasswordMatch = await bcrypt.compare(password, user.password)
	if (!isPasswordMatch) {
			throw new Error('Email ou Senha incorreta')
	}
	return user
}

const User = mongoose.model('User', UserSchema)
module.exports = User