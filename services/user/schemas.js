const userProfileOutput = {
  type: 'object',
  properties: {
    _id: { type: 'string' },
    email: { type: 'string' },
    user_type: { type: 'number' }
  }
}

const loginSchema = {
  body:{
    type: 'object',
    required: ['email', 'password'],
    properties: {
      email: { type: 'string' },
      password: { type: 'string', minLength: 6, maxLength: 15 }
    }
  },
  response:{
    200: {
      type: 'object',
      properties: {
        jwt: { type: 'string' },
        user: {
          type: 'object',
          properties: userProfileOutput.properties
        }
      },
      additionalProperties: false
    }
  }
}

const registerSchema = {
  body:{
    type: 'object',
    required: ['email','cpf', 'password', 'confirm_password', 'user_type'],
    properties: {
      email: { type: 'string' },
      cpf: { type: 'string', minLength: 11, maxLength: 11 },
      user_type: { type: 'number', enum: [0,1]},
      password: { type: 'string', minLength: 6, maxLength: 15 },
      confirm_password: { type: 'string', minLength: 6, maxLength: 15 }
    }
  },
  response: {
    200: {
      type: 'object',
      properties: {
        user: {
          type: 'object',
          properties: userProfileOutput.properties
        }
      },
      additionalProperties: false
    }
  }
}

module.exports = {
  loginSchema,
  registerSchema
}