const User = require('./Model/User')

exports.register = async (body) => {
	if(body.password !== body.confirm_password){
		throw new Error("Senhas divergentes")
	}
	try {
		const user = new User(body)
		const result = await user.save()
		return result
	} catch (e) {
		if (e.code === 11000){
			const error = e.errmsg.slice(60, 65)
			if (error === 'email')
				throw new Error('Email já cadastrado')
			else {
				throw new Error('CPF já cadastrado')
			}
		}
		throw e
	}
}

exports.login = async (email, password) => {
	const validateEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)
	if(!validateEmail){
		throw new Error("Email Inválido")
	}
	try {
		const user = await User.findByCredentials(email, password)
		return user
	} catch (e) {
		throw e
	}
}
