'use strict'

module.exports = {
  'LOGIN_ERROR': 'LOGIN_ERROR',
  'INTERNAL_ERROR': 'INTERNAL_ERROR',
  'INVALID_TOKEN': 'INVALID_TOKEN',
}
