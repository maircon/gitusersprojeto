# PayPrev Projeto

### Tabela de conteúdos
* [Servidor](#server)
* [Serviços](#services)
  * [User](#user-service)
  * [Git](#gitUser-service)
  * [List](#listsUsers-service)
### <a name=“server”><a/> Servidor

Para Iniciar o servidor rode o seguinte comando na raíz.  
`npm start`

### <a name=“Rotas”><a/> Rotas

BASE_URL_PRODUÇÃO = https://maircon-teste.herokuapp.com
BASE_URL_LOCAL = localhost:3001

user = {

    --- ROTA NÃO PROTEGIDA ---
    POST /api/user/login ( body-> email e password  )
    POST /api/user/register ( body -> email, cpf, password, confirm_password, user_type = 0(comum) ou 1(adm) )

    
}

gitUser = {

    --- ROTA PROTEGIDA, Apenas ADM ---
    GET /api/git/ ## BUSCA USUÁRIOS NA API DO GIT
    POST /api/git/ ## INSERE USUÁRIO DO GIT NO BANCO ( body: gitLogin = 'LOGIN DO USUÁRIO' )
    DELETE /api/git/:gitId ## DELETA USUÁRIO DO GIT SALVO NO BANCO ( params: gitId = 'ID DO USUÁRIO')

    --- ROTA NÃO PROTEGIDA, ADM E COMUM ---
    GET /api/git/users ## BUSCA USUÁRIO DO GIT SALVOS NO BANCO

    
}

listUsers = {

    --- ROTA PROTEGIDA, ADM E COMUM --- 
    GET /api/list/ ## BUSCA TODAS AS LISTAS
    POST /api/list/ ## INSERE NOVA LISTA ( body: listName = 'NOME DA LISTA' )
    GET /api/list/:listId ## BUSCA DE LISTA POR ID ( params: listId = 'ID DA LISTA' )
    PUT /api/list/:listId ## ATUALIZA LISTA POR ID ( params: listId = 'ID DA LISTA', body: listName = 'NOVO NOME DE LISTA')
    DELETE /api/list/:listId ## DELETA LISTA POR ID ( params: listId = 'ID DA LISTA' )
    POST /api/list/:listId/gitUser/:gitUserId ## INSERE USUARIO NA LISTA POR ID ( params: listId e gitUserId; body: tags = 'TAG QUE SERÁ SALVO JUNTO COM USUÁRIO NA LISTA' )
    PUT /api/list/:listId/gitUser/:gitUserId ## ATUALIZA USUARIO NA LISTA POR ID ( params: listId e gitUserId; body: tags = 'TAG QUE SERÁ SALVO JUNTO COM USUÁRIO NA LISTA' )
    DELETE /api/list/:listId/gitUser/:gitUserId ## DELETE USUARIO DA LISTA POR ID ( params: listId e gitUserId)
}
