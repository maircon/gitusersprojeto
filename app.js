const mongoose = require('mongoose')
const fp = require('fastify-plugin')

mongoose.connect('mongodb://payprev:teste123@ds261277.mlab.com:61277/payprev', { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false })

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("CONECTADO COM SUCESSO NO BANCO")
});

async function authenticator (fastify) {
  fastify
    .register(require('fastify-jwt'), {
      secret: 'TESTE',
      algorithms: ['RS256']
    })
}

async function decorateFastifyInstance (fastify) {
  fastify.decorate('authAdmPreHandler', async function auth (request, reply) {
    try {
      const user = await request.jwtVerify()
      if (user.user_type === 0){
        throw new Error('Usuario não é Adm')
      }
    } catch (err) {
      reply.send(err)
    }
  })
}

async function decorateFastifyInstance1 (fastify) {
  fastify.decorate('authPreHandler', async function auth (request, reply) {
    try {
      await request.jwtVerify()
    } catch (err) {
      reply.send(err)
    }
  })
}

module.exports = async function (fastify, opts) {
  fastify
    .register(fp(authenticator))
    .register(fp(decorateFastifyInstance))
    .register(fp(decorateFastifyInstance1))
    .register(require('./services/user'), { prefix: '/api/user' })
    .register(require('./services/gitUsers'), { prefix: '/api/git' })
    .register(require('./services/listsUsers'), { prefix: '/api/list' })
}